# USC2

## Install
```sh
sudo apt install gcc cmake flex
```

## Build
```sh
rm -rf ./build/* && cd build && cmake .. && make && cd ..
```

## Run
```sh 
./build/USC2 < test/hello.txt
```
