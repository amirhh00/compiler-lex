Digit	[0-9]
Letter [a-zA-Z_]

%{
#include <stdio.h>
#define SEMICOLON 100
#define BLOCKOPEN 101
#define BLOCKCLOSE 102
#define READ_STMT 104
#define PRINT_STMT 105
#define TRUE 106
#define FALSE 107
#define PLUS 108
#define MINUS 109
#define TIMES 110
#define DIVISION 111
#define OR 112
#define AND 113
#define ASSIGN 114
#define OPARENTHESIS 115
#define CPARENTHESIS 116
#define GT 117
#define LT 118
#define GTE 119
#define LTE 120
#define EQUAL 121
#define NOTEQUAL 122
#define BY 123
#define NOT 124
enum {ID=300, STRING, NUMBER};
enum {CHAR=500, INT, LONG, FLOAT, DOUBLE};
enum {CASE=600, DEFAULT, IF, ELSE, THEN, SWITCH, WHILE, DO, FOR, TO, CONTINUE, BREAK, RETURN, REPEAT, UNTIL};
%}

%%
"//"[^\n]*  {}
"break"			{ return(BREAK); }
"case"			{ return(CASE); }
"char"			{ return(CHAR); }
"continue"	{ return(CONTINUE); }
"default"		{ return(DEFAULT); }
"do"				{ return(DO); }
"double"		{ return(DOUBLE); }
"else"			{ return(ELSE); }
"float"			{ return(FLOAT); }
"for"				{ return(FOR); }
"if"				{ return(IF); }
"to"				{ return(TO); }
"then"			{ return(THEN); }
"int"				{ return(INT); }
"long"			{ return(LONG); }
"return"		{ return(RETURN); }
"switch"		{ return(SWITCH); }
"while"			{ return(WHILE); }
"repeat"		{ return(REPEAT); }
"until"			{ return(UNTIL); }
"print"			{ return(PRINT_STMT); }
"read"			{ return(READ_STMT); }
"true"			{ return(TRUE); }
"false"			{ return(FALSE); }
"by"				{ return(BY); }
"or"				{ return(OR); }
"and"				{ return(AND); }
"not"				{ return(NOT); }
{Letter}({Letter}|{Digit})*		{ return(ID); }
({Digit})+										{ return(NUMBER); }
\"(\\.|[^\\"])*\"		{ return(STRING); }
";"					{ return(SEMICOLON); }
"{"					{ return(BLOCKOPEN); }
"}"					{ return(BLOCKCLOSE); }
"-"					{ return(MINUS); }
"+"					{ return(PLUS); }
"*"					{ return(TIMES); }
"/"					{ return(DIVISION); }
":="				{ return(ASSIGN); }
"("					{ return(OPARENTHESIS); }
")"					{ return(CPARENTHESIS); }
">"					{ return(GT); }
"<"					{ return(LT); }
">="				{ return(GTE); }
"<="				{ return(LTE); }
"="					{ return(EQUAL); }
"<>"				{ return(NOTEQUAL); }
[ \t\n]			{ }
%%

int yywrap() {
	return(1);
}

int main(int argc, char **argv) {
	int token;
	while(token = yylex())
			printf("[%d, %s]\n", token, yytext);
	return 0;
}
